# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# SPDX-FileCopyrightText: 2015, 2017, 2019, 2021, 2023, 2024 A S Alam <aalam@users.sf.net>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-02-28 00:42+0000\n"
"PO-Revision-Date: 2024-01-03 14:34-0600\n"
"Last-Translator: A S Alam <aalam@punlinux.org>\n"
"Language-Team: Punjabi <kde-i18n-doc@kde.org>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: kcm.cpp:156 kcm.cpp:178
#, kde-format
msgid "Implementation error"
msgstr ""

#: kcm.cpp:204
#, kde-format
msgid "Outputs changed while trying to apply settings"
msgstr ""

#: output_model.cpp:96
#, kde-format
msgid "%1 Hz"
msgstr "%1 Hz"

#: output_model.cpp:660
#, kde-format
msgctxt "Width x height"
msgid "%1 × %2"
msgstr ""

#: output_model.cpp:678
#, fuzzy, kde-format
#| msgctxt "Width x height (aspect ratio)"
#| msgid "%1x%2 (%3:%4)"
msgctxt "Width x height (aspect ratio)"
msgid "%1 × %2 (%3:%4)"
msgstr "%1x%2 (%3:%4)"

#: output_model.cpp:759
#, kde-format
msgid "None"
msgstr "ਕੋਈ ਨਹੀਂ"

#: output_model.cpp:766
#, kde-format
msgid "Replicated by other output"
msgstr "ਹੋਰ ਆਉਟਪੁੱਟ ਦੀ ਹੂ-ਬਹੂ ਨਕਲ ਕੀਤੀ"

#: ui/main.qml:26
#, fuzzy, kde-format
#| msgid "Identify"
msgctxt ""
"@action:button Briefly show the display name in a popup label on each screen"
msgid "Identify Screens"
msgstr "ਪਛਾਣੋ"

#: ui/main.qml:41
#, kde-format
msgid "Keep display configuration?"
msgstr "ਡਿਸਪਲੇਅ ਸੰਰਚਨਾ ਰੱਖਣੀ ਹੈ?"

#: ui/main.qml:50
#, kde-format
msgid "Will revert to previous configuration in %1 second."
msgid_plural "Will revert to previous configuration in %1 seconds."
msgstr[0] "%1 ਸਕਿੰਟ ਵਿੱਚ ਪਿਛਲੀ ਸੰਰਚਨਾ ਨੂੰ ਬਹਾਲ ਕੀਤਾ ਜਾਵੇਗਾ।"
msgstr[1] "%1 ਸਕਿੰਟਾਂ ਵਿੱਚ ਪਿਛਲੀ ਸੰਰਚਨਾ ਨੂੰ ਬਹਾਲ ਕੀਤਾ ਜਾਵੇਗਾ।"

#: ui/main.qml:67
#, kde-format
msgid "&Keep"
msgstr "ਰੱਖੋ(&K)"

#: ui/main.qml:80
#, kde-format
msgid "&Revert"
msgstr "ਵਾਪਸ ਲਵੋ(&R)"

#: ui/main.qml:100
#, kde-format
msgctxt "@info"
msgid "All displays are disabled. Enable at least one."
msgstr "ਸਾਰੇ ਡਿਸਪਲੇਅ ਅਸਮਰੱਥ ਹਨ। ਘੱਟੋ-ਘੱਟ ਇੱਕ ਨੂੰ ਸਮਰੱਥ ਕਰੋ।"

#: ui/main.qml:102
#, kde-format
msgctxt "@info"
msgid ""
"Gaps between displays are not supported. Make sure all displays are touching."
msgstr "ਡਿਸਪਲੇਅ ਵਿਚਾਲੇ ਫ਼਼ਾਸਲੇ ਲਈ ਸਹਾਇਕ ਨਹੀਂ ਹੈ। ਯਕੀਨੀ ਬਣਾਓ ਕਿ ਸਾਰੇ ਡਿਸਪਲੇਅ ਛੂਹ ਰਹੇ ਹਨ।"

#: ui/main.qml:107
#, fuzzy, kde-format
#| msgid "Keep display configuration?"
msgctxt "The argument contains the reason for the failure"
msgid "Couldn’t apply display configuration: %1"
msgstr "ਡਿਸਪਲੇਅ ਸੰਰਚਨਾ ਰੱਖਣੀ ਹੈ?"

#: ui/main.qml:116
#, kde-format
msgid "A new output has been added. Settings have been reloaded."
msgstr "ਨਵੀਂ ਆਉਟਪੁੱਟ ਨੂੰ ਜੋੜਿਆ ਗਿਆ। ਸੈਟਿੰਗਾਂ ਨੂੰ ਮੁੜ-ਲੋਡ ਕੀਤਾ ਜਾ ਚੁੱਕਾ ਹੈ।"

#: ui/main.qml:118
#, kde-format
msgid "An output has been removed. Settings have been reloaded."
msgstr "ਆਉਟਪੁੱਟ ਨੂੰ ਹਟਾਇਆ ਗਿਆ। ਸੈਟਿੰਗਾਂ ਨੂੰ ਮੁੜ-ਲੋਡ ਕੀਤਾ ਜਾ ਚੁੱਕਾ ਹੈ।"

#: ui/main.qml:158
#, kde-format
msgid "No KScreen backend found. Please check your KScreen installation."
msgstr "ਕੋਈ KScreen ਬੈਕਐਂਡ ਨਹੀਂ ਮਿਲਿਆ। ਆਪਣੀ KScreen ਇੰਸਟਾਲੇਸ਼ਨ ਦੀ ਜਾਂਚ ਕਰੋ।"

#: ui/main.qml:167
#, kde-format
msgid "Outputs could not be saved due to error."
msgstr "ਗ਼ਲਤੀ ਦੇ ਕਰਕੇ ਆਉਟਪੁੱਟ ਨੂੰ ਸੰਭਾਲਿਆ ਨਹੀਂ ਜਾ ਸਕਿਆ।"

#: ui/main.qml:176
#, kde-format
msgid ""
"Global scale changes will come into effect after the system is restarted."
msgstr "ਗਲੋਬਲ ਸਕੇਲ ਤਬਦੀਲੀਆਂ ਸਿਸਟਮ ਦੇ ਮੁੜ-ਚਾਲੂ ਹੋਣ ਬਾਅਦ ਹੀ ਲਾਗੂ ਹੋਣਗੀਆਂ।"

#: ui/main.qml:182
#, kde-format
msgid "Restart"
msgstr "ਮੁੜ-ਚਾਲੂ ਕਰੋ"

#: ui/main.qml:200
#, kde-format
msgid "Display configuration reverted."
msgstr "ਡਿਸਪਲੇਅ ਸੰਰਚਨਾ ਵਾਪਸ ਲਈ ਗਈ ਹੈ।"

#: ui/main.qml:212
#, kde-format
msgctxt "@title:window"
msgid "Change Priorities"
msgstr "ਤਰਜੀਹਾਂ ਬਦਲੋ"

#: ui/main.qml:242 ui/OutputPanel.qml:45
#, kde-format
msgid "Primary"
msgstr "ਪ੍ਰਾਇਮਰੀ"

#: ui/main.qml:247
#, kde-format
msgid "Raise priority"
msgstr "ਤਰਜੀਹ ਵਧਾਓ"

#: ui/main.qml:257
#, kde-format
msgid "Lower priority"
msgstr "ਤਰਜੀਹ ਘਟਾਓ"

#: ui/Orientation.qml:14
#, kde-format
msgid "Orientation:"
msgstr "ਸਥਿਤੀ:"

#: ui/Orientation.qml:28 ui/OutputPanel.qml:162 ui/OutputPanel.qml:204
#, kde-format
msgid "Automatic"
msgstr "ਆਟੋਮੈਟਿਕ"

#: ui/Orientation.qml:43
#, kde-format
msgid "Only when in tablet mode"
msgstr "ਸਿਰਫ਼ ਜਦੋਂ ਟੇਬਲੇਟ ਢੰਗ ਵਿੱਚ ਹੈ"

#: ui/Orientation.qml:59
#, kde-format
msgid "Manual"
msgstr "ਖੁਦ"

#: ui/Output.qml:19
#, kde-format
msgctxt "Width, height, scale; separated with no-break space"
msgid "(%1 × %2, %3%)"
msgstr ""

#: ui/Output.qml:21
#, kde-format
msgctxt "Width, height; separated with no-break space"
msgid "(%1 × %2)"
msgstr ""

#: ui/Output.qml:216
#, kde-format
msgid "%1, %2"
msgstr ""

#: ui/Output.qml:242
#, kde-format
msgid "Replicas"
msgstr "ਹੂ-ਬਹੂ ਨਕਲ"

#: ui/OutputPanel.qml:27
#, kde-format
msgid "Enabled"
msgstr "ਸਮਰੱਥ ਹੈ"

#: ui/OutputPanel.qml:38
#, kde-format
msgid "Change Screen Priorities…"
msgstr "…ਸਕਰੀਨ ਤਰਜੀਹਾਂ ਬਦਲੋ"

#: ui/OutputPanel.qml:51
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This determines which screen your main desktop appears on, along with any "
"Plasma Panels in it. Some older games also use this setting to decide which "
"screen to appear on.<nl/><nl/>It has no effect on what screen notifications "
"or other windows appear on."
msgstr ""

#: ui/OutputPanel.qml:56
#, kde-format
msgid "Resolution:"
msgstr "ਰੈਜ਼ੋਲੂਸ਼ਨ:"

#: ui/OutputPanel.qml:77
#, fuzzy, kde-format
#| msgctxt "@info"
#| msgid "&quot;%1&quot; is the only resolution supported by this display."
msgctxt "@info"
msgid "“%1” is the only resolution supported by this display."
msgstr "ਇਸ ਡਿਸਪਲੇਅ ਵਲੋਂ ਸਿਰਫ਼ &quot;%1&quot; ਰੀਫਰੈਸ਼ ਰੇਟ ਸਹਾਇਕ ਹੈ।"

#: ui/OutputPanel.qml:87
#, kde-format
msgid "Scale:"
msgstr "ਸਕੇਲ:"

#: ui/OutputPanel.qml:93 ui/Panel.qml:82
#, kde-format
msgctxt "@info accessible description of slider value"
msgid "in percent of regular scale"
msgstr ""

#: ui/OutputPanel.qml:120 ui/Panel.qml:113
#, kde-format
msgctxt "Global scale factor expressed in percentage form"
msgid "%1%"
msgstr "%1%"

#: ui/OutputPanel.qml:132
#, kde-format
msgid "Refresh rate:"
msgstr "ਤਾਜ਼ਾ ਕਰਨ ਰੇਟ:"

#: ui/OutputPanel.qml:153
#, fuzzy, kde-format
#| msgid "\"%1\" is the only refresh rate supported by this display."
msgctxt "@info"
msgid "“%1” is the only refresh rate supported by this display."
msgstr "ਇਸ ਡਿਸਪਲੇਅ ਵਲੋਂ ਸਿਰਫ਼ \"%1\" ਰੀਫਰੈਸ਼ ਰੇਟ ਸਹਾਇਕ ਹੈ।"

#: ui/OutputPanel.qml:158
#, kde-format
msgid "Adaptive sync:"
msgstr ""

#: ui/OutputPanel.qml:161
#, kde-format
msgid "Never"
msgstr "ਕਦੇ ਨਹੀਂ"

#: ui/OutputPanel.qml:163
#, kde-format
msgid "Always"
msgstr "ਹਮੇਸ਼ਾ"

#: ui/OutputPanel.qml:174
#, kde-format
msgid "Overscan:"
msgstr "ਓਵਰ-ਸਕੈਨ:"

#: ui/OutputPanel.qml:185
#, kde-format
msgctxt "Overscan expressed in percentage form"
msgid "%1%"
msgstr "%1%"

#: ui/OutputPanel.qml:191
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Determines how much padding is put around the image sent to the display to "
"compensate for part of the content being cut off around the edges.<nl/><nl/"
">This is sometimes needed when using a TV as a screen"
msgstr ""

#: ui/OutputPanel.qml:196
#, kde-format
msgid "RGB range:"
msgstr "RGB ਰੇਜ਼:"

#: ui/OutputPanel.qml:205
#, kde-format
msgid "Full"
msgstr "ਪੂਰਾ"

#: ui/OutputPanel.qml:206
#, kde-format
msgid "Limited"
msgstr "ਸੀਮਤ"

#: ui/OutputPanel.qml:216
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Determines whether or not the range of possible color values needs to be "
"limited for the display. This should only be changed if the colors on the "
"screen look washed out."
msgstr ""

#: ui/OutputPanel.qml:221
#, fuzzy, kde-format
#| msgctxt "@label:textbox"
#| msgid "Color Profile:"
msgctxt "@label:listbox"
msgid "Color profile:"
msgstr "ਰੰਗ ਪਰਫਾਇਲ:"

#: ui/OutputPanel.qml:232
#, fuzzy, kde-format
#| msgid "None"
msgctxt "@item:inlistbox color profile"
msgid "None"
msgstr "ਕੋਈ ਨਹੀਂ"

#: ui/OutputPanel.qml:237
#, fuzzy, kde-format
#| msgctxt "@title:window"
#| msgid "Select ICC Profile"
msgctxt "@item:inlistbox color profile"
msgid "ICC profile"
msgstr "ICC ਪਰੋਫਾਇਲ ਚੁਣੋ"

#: ui/OutputPanel.qml:242
#, kde-format
msgctxt "@item:inlistbox color profile"
msgid "Built-in"
msgstr ""

#: ui/OutputPanel.qml:260
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Use the color profile built into the screen itself, if present. Note that "
"built-in color profiles are sometimes wrong, and often inaccurate. For "
"optimal color fidelity, calibration using a colorimeter is recommended."
msgstr ""

#: ui/OutputPanel.qml:264
#, kde-format
msgctxt "@info:tooltip"
msgid "The screen's built-in color profile is always used with HDR."
msgstr ""

#: ui/OutputPanel.qml:277
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter ICC profile path…"
msgstr "ICC ਪਰੋਫਾਇਲ ਦਾ ਮਾਰਗ ਦਿਓ…"

#: ui/OutputPanel.qml:293
#, kde-format
msgctxt "@action:button"
msgid "Select ICC profile…"
msgstr "ICC ਪਰੋਫਾਇਲ ਚੁਣੋ…"

#: ui/OutputPanel.qml:304
#, kde-format
msgid "Opens a file picker for the ICC profile"
msgstr "ICC ਪਰੋਫਾਇਲ ਲਈ ਫਾਇਲ ਚੋਣਕਾਰ ਨੂੰ ਖੋਲ੍ਹਦਾ ਹੈ"

#: ui/OutputPanel.qml:313
#, kde-format
msgctxt "@title:window"
msgid "Select ICC Profile"
msgstr "ICC ਪਰੋਫਾਇਲ ਚੁਣੋ"

#: ui/OutputPanel.qml:338
#, kde-format
msgctxt "@info:tooltip"
msgid "ICC profiles aren’t compatible with HDR yet"
msgstr ""

#: ui/OutputPanel.qml:346
#, kde-format
msgctxt "@label"
msgid "High Dynamic Range:"
msgstr "ਹਾਈ ਡਾਇਨੈਮਿਕ ਰੇਜ਼:"

#: ui/OutputPanel.qml:353
#, fuzzy, kde-format
#| msgctxt "@option:check"
#| msgid "Enable HDR"
msgctxt "@option:check"
msgid "Enable &HDR"
msgstr "HDR ਸਮਰੱਥ"

#: ui/OutputPanel.qml:359
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"HDR allows compatible applications to show brighter and more vivid colors"
msgstr ""

#: ui/OutputPanel.qml:367
#, kde-format
msgctxt "@label:listbox"
msgid "Color accuracy:"
msgstr ""

#: ui/OutputPanel.qml:376
#, kde-format
msgctxt "@item:inlistbox tradeoff between efficiency and color accuracy"
msgid "Prefer efficiency"
msgstr ""

#: ui/OutputPanel.qml:377
#, kde-format
msgctxt "@item:inlistbox tradeoff between efficiency and color accuracy"
msgid "Prefer color accuracy"
msgstr ""

#: ui/OutputPanel.qml:387
#, kde-format
msgctxt "@info:tooltip"
msgid "This setting can have a large impact on performance"
msgstr ""

#: ui/OutputPanel.qml:398
#, fuzzy, kde-format
#| msgctxt "@label"
#| msgid "SDR Color Intensity:"
msgctxt "@label"
msgid "sRGB color intensity:"
msgstr "SDR ਰੰਗ ਇਨਟੈਂਸਟੀ:"

#: ui/OutputPanel.qml:426
#, kde-format
msgctxt "Color intensity factor expressed in percentage form"
msgid "%1%"
msgstr "%1%"

#: ui/OutputPanel.qml:434
#, kde-format
msgctxt "@info:tooltip"
msgid "Increases the intensity of sRGB content on the screen"
msgstr ""

#: ui/OutputPanel.qml:445
#, fuzzy, kde-format
#| msgctxt "@label"
#| msgid "SDR Brightness:"
msgctxt "@label"
msgid "Maximum SDR brightness:"
msgstr "SDR ਚਮਕ:"

#: ui/OutputPanel.qml:466
#, kde-format
msgctxt "@info:tooltip"
msgid "Sets the maximum brightness for the normal brightness slider"
msgstr ""

#: ui/OutputPanel.qml:477
#, fuzzy, kde-format
#| msgctxt "@label"
#| msgid "SDR Brightness:"
msgctxt "@label"
msgid "Brightness:"
msgstr "SDR ਚਮਕ:"

#: ui/OutputPanel.qml:500
#, kde-format
msgid "Replica of:"
msgstr "ਇਸ ਦੀ ਹੂ-ਬਹੂ ਨਕਲ:"

#: ui/Panel.qml:27
#, kde-format
msgid "Device:"
msgstr "ਡਿਵਾਈਸ:"

#: ui/Panel.qml:75
#, kde-format
msgid "Global scale:"
msgstr "ਗਲੋਬਲ ਸਕੇਲ:"

#: ui/Panel.qml:137 ui/Panel.qml:157
#, kde-format
msgid "Legacy applications (X11):"
msgstr "ਪੁਰਾਤਨ ਐਪਲੀਕੇਸ਼ਨਾਂ (X11):"

#: ui/Panel.qml:142
#, kde-format
msgctxt "The apps themselves should scale to fit the displays"
msgid "Apply scaling themselves"
msgstr "ਸਕੇਲਿੰਗਾਂ ਨੂੰ ਖੁਦ ਲਾਗੂ ਕਰੋ"

#: ui/Panel.qml:147
#, kde-format
msgid ""
"Legacy applications that support scaling will use it and look crisp, however "
"those that don’t will not be scaled at all."
msgstr ""

#: ui/Panel.qml:158
#, kde-format
msgctxt "The system will perform the x11 apps scaling"
msgid "Scaled by the system"
msgstr "ਸਿਸਟਮ ਰਾਹੀਂ ਸਕੇਲ ਕੀਤਾ"

#: ui/Panel.qml:163
#, kde-format
msgid ""
"All legacy applications will be scaled by the system to the correct size, "
"however they will always look slightly blurry."
msgstr ""

#: ui/Panel.qml:168
#, kde-format
msgctxt "@label"
msgid "Screen tearing:"
msgstr "ਸਕਰੀਨ ਟੀਅਰਿੰਗ:"

#: ui/Panel.qml:171
#, kde-format
msgctxt ""
"@option:check The thing being allowed in fullscreen windows is screen tearing"
msgid "Allow in fullscreen windows"
msgstr "ਪੂਰੀ ਸਕਰੀਨ ਵਿੰਡੋਆਂ ਵਿੱਚ ਮਨਜ਼ੂਰ"

#: ui/Panel.qml:176
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Screen tearing reduces latency with most displays. Note that not all "
"graphics drivers support this setting."
msgstr ""
"ਸਕਰੀਨ ਟੀਅਰਿੰਗ ਬਹੁਤੇ ਡਿਸਪਲੇਅ ਤੋਂ ਦੇਰੀ (latency) ਨੂੰ ਘਟਾਉਂਦੀ ਹੈ। ਯਾਦ ਰੱਖੋ ਕਿ ਸਾਰੇ ਗਰਾਫਿਕਸ "
"ਡਰਾਇਵਰ ਇਸ ਸੈਟਿੰਗ ਲਈ ਸਹਾਇਕ ਨਹੀਂ ਹੁੰਦੇ ਹਨ।"

#: ui/Panel.qml:190
#, kde-format
msgid ""
"The global scale factor is limited to multiples of 6.25% to minimize visual "
"glitches in applications using the X11 windowing system."
msgstr ""
"ਗਲੋਬਲ ਸਕੇਲ ਗੁਣਾਂਕ X11 ਵਿੰਡਿੰਗ ਸਿਸਟਮ ਵਰਤ ਕੇ ਐਪਲੀਕੇਸ਼ਨ ਵਿੱਚ 6.25% ਦੀ ਗੁਣਾਂ ਤੋਂ ਘੱਟੋ-ਘੱਟ ਦਿੱਖ "
"ਸਮੱਸਿਆਵਾਂ ਤੱਕ ਸੀਮਤ ਹੈ।"

#: ui/RotationButton.qml:54
#, kde-format
msgid "90° Clockwise"
msgstr "90° ਸੱਜੇ ਦਾਅ"

#: ui/RotationButton.qml:58
#, kde-format
msgid "Upside Down"
msgstr "ਉਲਟਾ"

#: ui/RotationButton.qml:62
#, kde-format
msgid "90° Counterclockwise"
msgstr "90° ਖੱਬੇ ਦਾਅ"

#: ui/RotationButton.qml:67
#, kde-format
msgid "No Rotation"
msgstr "ਘੁੰਮਾਉਣਾ ਨਹੀਂ"

#: ui/ScreenView.qml:50
#, kde-format
msgid "Drag screens to re-arrange them"
msgstr "ਸਕਰੀਨਾਂ ਨੂੰ ਤਰਤੀਬ ਦੇਣ ਲਈ ਉਹਨਾਂ ਨੂੰ ਖਿੱਚੋ"

#~ msgctxt "Width x height"
#~ msgid "%1x%2"
#~ msgstr "%1x%2"

#~ msgid "Save displays' properties:"
#~ msgstr "ਡਿਸਪਲੇਅ ਦੀਆਂ ਵਿਸ਼ੇਸ਼ਤਾਵਾਂ ਨੂੰ ਸੰਭਾਲੋ:"

#~ msgid "For any display arrangement"
#~ msgstr "ਕਿਸੇ ਵੀ ਡਿਸਪਲੇਅ ਇੰਤਜ਼ਾਮ ਲਈ"

#~ msgid "For only this specific display arrangement"
#~ msgstr "ਸਿਰਫ਼ ਇਸ ਖਾਸ ਡਿਸਪਲੇਅ ਇੰਤਜ਼ਾਮ ਲਈ"

#~ msgid ""
#~ "Are you sure you want to disable all outputs? This might render the "
#~ "device unusable."
#~ msgstr ""
#~ "ਕੀ ਤੁਸੀਂ ਸਭ ਆਉਟਪੁੱਟਾਂ ਨੂੰ ਅਸਮਰੱਥ ਕਰਨਾ ਚਾਹੁੰਦੇ ਹੋ? ਇਹ ਡਿਵਾਈਸ ਨੂੰ ਬੇਕਾਰ ਨੂੰ ਰੈਂਡਰ ਕਰ ਸਕਦਾ ਹੈ।"

#~ msgid "Disable All Outputs"
#~ msgstr "ਸਭ ਆਉਟਪੁੱਟਾਂ ਨੂੰ ਅਸਮਰੱਥ ਕਰੋ"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ਅ ਸ ਆਲਮ"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "alam.yellow@gmail.com"

#~ msgid "Display Configuration"
#~ msgstr "ਡਿਸਪਲੇਅ ਸੰਰਚਨਾ"

#~ msgid "Manage and configure monitors and displays"
#~ msgstr "ਮਾਨੀਟਰ ਅਤੇ ਡਿਸਪਲੇਅ ਦਾ ਇੰਤਜ਼ਾਮ ਅਤੇ ਸੰਰਚਨਾ ਕਰੋ"

#~ msgid "Copyright © 2019 Roman Gilg"
#~ msgstr "ਕਾਪੀਰਾਈਟ © 2019 Roman Gilg"

#~ msgid "Roman Gilg"
#~ msgstr "ਰੋਮਨ ਗਿਲਗ"

#~ msgid "Developer"
#~ msgstr "ਡਿਵੈਲਪਰ"

#~ msgid "Maintainer"
#~ msgstr "ਪਰਬੰਧਕ"

#, fuzzy
#~| msgctxt "@title:window"
#~| msgid "Disable All Outputs"
#~ msgid "Disable all outputs"
#~ msgstr "ਸਭ ਆਉਟਪੁੱਟਾਂ ਨੂੰ ਅਸਮਰੱਥ ਕਰੋ"

#~ msgid "Configuration for displays"
#~ msgstr "ਡਿਸਪਲੇਅ ਲਈ ਸੰਰਚਨਾ ਕਰੋ"

#, fuzzy
#~| msgid "Advanced Settings"
#~ msgid "Arrangement settings"
#~ msgstr "ਤਕਨੀਕੀ ਸੈਟਿੰਗਾਂ"

#~ msgid "Tip: Hold Ctrl while dragging a display to disable snapping"
#~ msgstr "ਗੁਰ: ਡਿਪਲੇਅ ਨੂੰ ਖਿੱਚਣ ਦੇ ਦੌਰਾਨ ਸਨੈਪ ਕਰਨ ਨੂੰ ਅਸਮਰੱਥ ਕਰਨ ਵਾਸਤੇ Ctrl ਨੂੰ ਦੱਬੀ ਰੱਖੋ"

#~ msgid "Warning: There are no active outputs!"
#~ msgstr "ਸਾਵਧਾਨ: ਕੋਈ ਵੀ ਸਰਗਰਮ ਆਉਟਪੁਟ ਨਹੀਂ ਹੈ!"

#~ msgid "Your system only supports up to %1 active screen"
#~ msgid_plural "Your system only supports up to %1 active screens"
#~ msgstr[0] "ਤੁਹਾਡਾ ਸਿਸਟਮ ਕੇਵਲ %1 ਸਰਗਰਮ ਸਕਰੀਨ ਲਈ ਹੀ ਸਹਾਇਕ ਹੈ"
#~ msgstr[1] "ਤੁਹਾਡਾ ਸਿਸਟਮ ਕੇਵਲ %1 ਸਰਗਰਮ ਸਕਰੀਨਾਂ ਲਈ ਹੀ ਸਹਾਇਕ ਹੈ"

#~ msgid "Unified Outputs"
#~ msgstr "ਆਉਟਪੁੱਟਾਂ ਨੂੰ ਇਕਸਾਰ ਕੀਤਾ"

#~ msgid "Laptop Screen"
#~ msgstr "ਲੈਪਟਾਪ ਸਕਰੀਨ"

#~ msgid "(c), 2012-2013 Daniel Vrátil"
#~ msgstr "(c), 2012-2013 Daniel Vrátil"

#~ msgid "Daniel Vrátil"
#~ msgstr "ਡੈਨੀਅਲ ਵਰਾਟਿਲ"

#~ msgid "&Reconfigure"
#~ msgstr "ਮੁੜ-ਸੰਰਚਨਾ ਕਰੋ(&R)"

#~ msgid ""
#~ "Sorry, your configuration could not be applied.\n"
#~ "\n"
#~ "Common reasons are that the overall screen size is too big, or you "
#~ "enabled more displays than supported by your GPU."
#~ msgstr ""
#~ "ਅਫ਼ਸੋਸ, ਤੁਹਾਡੀ ਸੰਰਚਨਾ ਲਾਗੂ ਨਹੀਂ ਕੀਤੀ ਜਾ ਸਕੀ।\n"
#~ "\n"
#~ "ਸਕਰੀਨ ਦਾ ਪੂਰਾ ਆਕਾਰ ਬਹੁਤ ਵੱਡਣਾ ਹੋਣ ਜਾਂ ਤੁਹਾਡੇ ਵਲੋਂ ਆਪਣੇ GPU ਵਲੋਂ ਸਹਾਇਕ ਡਿਸਪਲੇਅ ਤੋਂ ਵੱਧ ਨੂੰ "
#~ "ਸਮਰੱਥ ਕਰਨਾ ਆਮ ਕਾਰਨ ਹਨ।"

#~ msgctxt "@title:window"
#~ msgid "Unsupported Configuration"
#~ msgstr "ਗ਼ੈਰ-ਸਹਾਇਕ ਸੰਰਚਨਾ"

#~ msgid "KCM Test App"
#~ msgstr "KCM ਟੈਸਟ ਐਪ"

#~ msgid "Unify Outputs"
#~ msgstr "ਆਉਟਪੁੱਟ ਨੂੰ ਇਕਰੂਪ ਕਰੋ"

#~ msgid "Scale Display"
#~ msgstr "ਡਿਸਪਲੇਅ ਨੂੰ ਸਕੇਲ ਕਰੋ"

#~ msgid "Display:"
#~ msgstr "ਡਿਸਪਲੇਅ:"

#~ msgid "No available resolutions"
#~ msgstr "ਕੋਈ ਰੈਜ਼ੋਲੂਸ਼ਨ ਮੌਜੂਦ ਨਹੀਂ"

#~ msgid "Tab 1"
#~ msgstr "ਟੈਬ 1"

#~ msgid "Group Box"
#~ msgstr "ਗਰੁੱਪ ਬਾਕਸ"

#~ msgid "Radio button"
#~ msgstr "ਰੇਡੀਓ ਬਟਨ"

#~ msgid "Checkbox"
#~ msgstr "Checkbox"

#~ msgid "Button"
#~ msgstr "ਬਟਨ"

#~ msgid "Combobox"
#~ msgstr "Combobox"

#~ msgid "Tab 2"
#~ msgstr "ਟੈਬ 2"

#~ msgid "Normal"
#~ msgstr "ਸਧਾਰਨ"

#~ msgid "No Primary Output"
#~ msgstr "ਕੋਈ ਪ੍ਰਾਇਮਰੀ ਆਉਟਪੁੱਟ ਨਹੀਂ"

#~ msgid "Break Unified Outputs"
#~ msgstr "ਇਕਰੂਪ ਆਉਟਪੁੱਟਾਂ ਨੂੰ ਵੱਖ-ਵੱਖ ਕਰੋ"

#~ msgid "Active profile"
#~ msgstr "ਸਰਗਰਮ ਪਰੋਫਾਇਲ"

#~ msgid "Dialog"
#~ msgstr "ਡਾਈਲਾਗ"

#~ msgid "TextLabel"
#~ msgstr "TextLabel"

#~ msgid "No primary screen"
#~ msgstr "ਕੋਈ ਪ੍ਰਾਇਮਰੀ ਸਕਰੀਨ ਨਹੀਂ"
