# Spanish translations for kscreen.po package.
# Copyright (C) 2015 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2015.
# Eloy Cuadra <ecuadra@eloihr.net>, 2015, 2016, 2018, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kscreen\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-28 01:40+0000\n"
"PO-Revision-Date: 2023-01-26 11:43+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#: osdaction.cpp:27
#, kde-format
msgid "Switch to external screen"
msgstr "Cambiar a la pantalla externa"

#: osdaction.cpp:28
#, kde-format
msgid "Switch to laptop screen"
msgstr "Cambiar a la pantalla del portátil"

#: osdaction.cpp:29
#, kde-format
msgid "Unify outputs"
msgstr "Unificar las salidas"

#: osdaction.cpp:30
#, kde-format
msgid "Extend to left"
msgstr "Extender hacia la izquierda"

#: osdaction.cpp:31
#, kde-format
msgid "Extend to right"
msgstr "Extender hacia la derecha"

#: osdaction.cpp:32
#, kde-format
msgid "Leave unchanged"
msgstr "Dejar sin cambios"

#: utils.cpp:21
#, kde-format
msgid "Built-in Screen"
msgstr "Pantalla integrada"

#~ msgid "Switch Display"
#~ msgstr "Cambiar pantalla"

#~ msgctxt "OSD text after XF86Display button press"
#~ msgid "No External Display"
#~ msgstr "Ninguna pantalla externa"

#~ msgctxt "OSD text after XF86Display button press"
#~ msgid "Changing Screen Layout"
#~ msgstr "Cambiando la disposición de pantallas"

#, fuzzy
#~| msgctxt "OSD text after XF86Display button press"
#~| msgid "No External Display"
#~ msgctxt "osd when displaybutton is pressed"
#~ msgid "Cloned Display"
#~ msgstr "Ninguna pantalla externa"

#, fuzzy
#~| msgctxt "OSD text after XF86Display button press"
#~| msgid "No External Display"
#~ msgctxt "osd when displaybutton is pressed"
#~ msgid "External Only"
#~ msgstr "Ninguna pantalla externa"
